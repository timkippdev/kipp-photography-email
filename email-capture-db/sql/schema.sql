CREATE DATABASE kipp_photography;

use kipp_photography;

CREATE TABLE events (
  id int auto_increment primary key,
  name varchar(100) not null
);

CREATE TABLE event_data (
  id int auto_increment primary key,
  event_id int not null,
  email varchar(255) not null,
  photo_choices varchar(255)
);

INSERT INTO events (name) VALUES ('Heritage Days 2018');