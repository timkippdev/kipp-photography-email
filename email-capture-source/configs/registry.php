<?php

$app = \Intersect\Application::instance();

return [
    'classes' => [],
    'singletons' => [],
    'events' => [],
    'commands' => []
];
