<?php

return [
    'GET' => [
    	'/results' => 'App\Controllers\IndexController#results',
    	'/' => 'App\Controllers\IndexController#index'
    ],
    'POST' => [
        '/' => 'App\Controllers\IndexController#process'
    ]
];
