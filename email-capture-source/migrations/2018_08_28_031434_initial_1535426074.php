<?php

use Intersect\Database\Migrations\AbstractMigration;
use Intersect\Database\Schema\Schema;

class Initial1535426074 extends AbstractMigration {

    /**
     * Run the migration
     */
    public function up()
    {
        $eventData = new \Intersect\Database\Schema\Table('event_data');
        $eventData->addColumn(new \Intersect\Database\Schema\Column('id', 'int', false, null, true));
        $eventData->addColumn(new \Intersect\Database\Schema\Column('event_id', 'int'));
        $eventData->addColumn(new \Intersect\Database\Schema\Column('email', 'varchar(255)'));
        $eventData->addColumn(new \Intersect\Database\Schema\Column('choices', 'varchar(255)'));

        Schema::createTable($eventData);
    }

    /**
     * Reverse the migration
     */
    public function down()
    {
        //
    }

}