<?php

namespace App\Controllers;

use App\Models\Event;
use App\Models\EventData;
use Intersect\Core\AbstractController;
use Intersect\Database\Query\QueryParameters;
use Intersect\Http\Response\TwigResponse;
use Intersect\Storage\SessionStorage;

class IndexController extends AbstractController {

    /** @var SessionStorage */
    private $sessionStorage;

    public function __construct()
    {
        $this->sessionStorage = new SessionStorage();
        $this->sessionStorage->start();
    }

    public function index()
    {
        $changeEvent = $this->getRequest()->get('change');
        if (!is_null($changeEvent))
        {
            $this->sessionStorage->clear('current-event');
        }

        $eventId = $this->getRequest()->get('event');
        if (!empty($eventId))
        {
            $this->sessionStorage->write('current-event', $eventId);

            header('Location: /');
            exit();
        }

        $currentEventId = $this->sessionStorage->read('current-event');
        if (is_null($currentEventId))
        {
            return new TwigResponse('events.twig', [
                'events' => Event::find()
            ]);
        }

        $errorKey = $this->getRequest()->get('e');
        $errorMessage = false;
        if (!is_null($errorKey))
        {
            if ($errorKey == 'mf')
            {
                $errorMessage = 'Please field in both fields.';
            }
            else if ($errorKey == 'ev')
            {
                $errorMessage = 'Something unexpected went wrong.';
            }
            else if ($errorKey == 'ed')
            {
                $errorMessage = 'There was a problem saving your submission';
            }
        }

        $successKey = $this->getRequest()->get('s');
        $success = (!is_null($successKey));

        if ($success)
        {
            $this->sessionStorage->clear('email');
            $this->sessionStorage->clear('choices');
        }

        return new TwigResponse('welcome.twig', [
            'errorMessage' => $errorMessage,
            'currentEvent' => Event::findById($currentEventId),
            'success' => $success,
            'email' => $this->sessionStorage->read('email'),
            'choices' => $this->sessionStorage->read('choices')
        ]);
    }

    public function process()
    {
        $email = trim($this->getRequest()->post('email'));
        $choices = trim($this->getRequest()->post('choices'));
        $eventId = $this->sessionStorage->read('current-event');

        $this->sessionStorage->write('email', $email);
        $this->sessionStorage->write('choices', $choices);

        if (empty($email) || empty($choices))
        {
            header('Location: /?e=mf');
            exit();
        }

        if (empty($eventId))
        {
            header('Location: /?e=ev');
            exit();
        }

        $allChoices = [];
        foreach (explode(' ', $choices) as $choice)
        {
            if (!empty($choice))
            {
                $allChoices[] = $choice;
            }
        }

        $eventData = new EventData();
        $eventData->event_id = $eventId;
        $eventData->email = $email;
        $eventData->photo_choices = implode(',', $allChoices);

        $createdEventData = $eventData->save();

        if (is_null($createdEventData))
        {
            header('Location: /?e=ed');
            exit();
        }

        header('Location: /?s');
        exit();
    }

    public function results()
    {
        $currentEventId = $this->sessionStorage->read('current-event');
        $eventId = $this->getRequest()->get('event');

        $params = new QueryParameters();
        $params->equals('event_id', $eventId);

        return new TwigResponse('results.twig', [
            'eventData' => EventData::find($params),
            'currentEvent' => Event::findById($currentEventId),
            'events' => Event::find(),
            'event' => Event::findById($eventId)
        ]);
    }

}